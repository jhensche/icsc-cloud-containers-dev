#!/bin/sh

set -e
set -u
set -o pipefail

cat README.md | \
    grep -v -F 'Click here to see the solution' | \
    sed -r 's/<!-- (.+) -->/\1/p' \
        > temp.md

cat - temp.md > input.md <<EOF
---
title: "iCSC 2023 - Cloud & Containers - Exercises"
author: "Jack Henschel"
date: "2023-03-06"
keywords: [kubernetes, cloud, openshift, python]
lang: "en"
listings-no-page-break: true
listings-disable-line-numbers: true
...
EOF

podman run -u 0:0 --rm -v $PWD:/data docker.io/pandoc/extra input.md -o Instructions.pdf --template eisvogel --listings --toc

echo "Wrote PDF to Instructions.pdf"

rm temp.md input.md
